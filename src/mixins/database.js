import { mapState, mapMutations } from 'vuex'
import moment from 'moment'
import { db } from '../main.js'
export const database = {
	data(){
		return{
			nameModel: 'pacientes'
		}
	},
	computed:{
		...mapState('Test',['rasgos','idPaciente']),
	},
	methods:{
		...mapMutations('Test',['LlenarIDPaciente','ExisteUsuario','llenarDatosPaciente']),
		...mapMutations('Pacientes',['LlenarData','LlenaPacienteTest']),
		...mapMutations('Grupos',['LlenarPacientesGrupos']),
		consultaEmail(email){
			db.collection(this.nameModel).where("email", "==", email).get()
			.then((querySnapshot)=>{
				this.ExisteUsuario(false)
				querySnapshot.forEach(()=>{
					this.ExisteUsuario(true)
				});
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
		},
		add(name, email, TestGrupo){
			var objeto = {
				name: name,
				email: email,
				TestGrupo: TestGrupo,
				created_at: moment().format('DD-MM-YYYY')
			}
			db.collection(this.nameModel).add(objeto)
			.then((docRef)=> {
				this.LlenarIDPaciente(docRef.id)
				this.llenarDatosPaciente(objeto)
			})
			.catch(function(error) {
				console.error("Error adding document: ", error);
			});
		},
		updated(){
			db.collection(this.nameModel).doc(this.idPaciente).set({
				name: "Los Angeles",
				state: "CA",
				country: "USA"
			})
			.then(function() {
				console.log("Document successfully written!");
			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
			});
		},
		get(){
			var data = []
			db.collection(this.nameModel).get().then((querySnapshot) => {
				querySnapshot.forEach((doc) => {
					data.push({
						id: doc.id,
						datos: doc.data()
					})
				});
			});
			this.LlenarData(data)
		},
		delet(id){
			db.collection(this.nameModel).doc(id).delete().then(function() {
				console.log("Document successfully deleted!");
				alert('Eliminado')
				location.reload();
			}).catch(function(error) {
				console.error("Error removing document: ", error);
			});
		},
		getPaciente(id){
			var data = {}
			db.collection(this.nameModel).doc(id).get().then((doc)=> {
				data = doc.data()
				data.id = doc.id
				this.LlenaPacienteTest(data)
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
		},
		getPacientesGrupos(grupoID){
			var data = []
			var dat = {}
			db.collection(this.nameModel).where('TestGrupo', "==", grupoID).get().then((querySnapshot)=> {
				querySnapshot.forEach((doc)=> {
					dat = doc.data()
					dat.id = doc.id
					data.push(dat)
				});
				this.LlenarPacientesGrupos(data)
			})
			.catch(function(error) {
				console.log("Error getting documents: ", error);
			});
		}
	}
}
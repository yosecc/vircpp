import swal from 'sweetalert';
import { db } from '../main.js'
export default {
	namespaced: true,
	state:{
		rasgos:[],
		paso: null,
		idPaciente: null,
		datosPaciente: {},
		existe: false,
		ultimoPaso: false,
		categorias:['ETIOLOGÍA','SISTEMA DE CREENCIAS','DINÁMICA AFECTIVA','FÍSICO-ENERGÉTICO'],
		subcategoriasRigidos:['HISTERICO','PASIVO FEMENINO','MASCULINO AGRESIVO','FALICO-NARCISISTA'],
		backgrounds: [{id:0, color:'#7AE73C'},{id:1, color:'#3CD0E7'},{id:2, color:'#A93CE7'},{id:3, color:'#E7533C'},{id:4, color:'#E51FC1'}],
		rasgosTipos:['ESQUIZOIDE','ORAL','PSICOPÁTICO','RÍGIDO','MASOQUISTA'],
	},
	getters:{
		rasgosAleatorios: state => {
			return state.rasgos.slice(0).sort(function(){return Math.random() - 0.5})
		}
	},
	mutations:{
		responde(context, val){
			if (val != false) {
				context.rasgos.map((e)=>{
					if (e.id == val) {
						e.selected = true
					}
				})
			}
		},
		cambioPaso(context){
			if (context.paso == null) {
				context.paso = 0
			}else{
				context.paso++
			}
		},
		LlenarIDPaciente(context, val){
			context.idPaciente = val
		},
		cambiarUltimoPaso(context){
			context.ultimoPaso = true
		},
		modificaRasgos(context, val){
			context.rasgos = val
		},
		ExisteUsuario(context,val){
			context.existe = val
		},
		llenarDatosPaciente(context, val){
			context.datosPaciente = val
		}
	},
	actions:{
		cambioPasoAction(context){
			context.commit('cambioPaso')
			if (context.state.paso >= context.state.rasgos.length) {
				context.commit('cambiarUltimoPaso')
				context.dispatch('updated')
			}
		},
		updated(context){
			if (context.state.datosPaciente.name == '' || context.state.datosPaciente.name == null) {
				swal("Espera :(", "Lo sentimos no hemos logrado registrar tus datos. Intentalo nuevamente", "error").then(()=>{
				location.reload();
				})
				return false
			}
			db.collection("pacientes").doc(context.state.idPaciente).set({
				rasgos: context.state.rasgos
			},{ merge: true })
			.then(function() {
				console.log("Document successfully written!");
			})
			.catch(function(error) {
				console.error("Error writing document: ", error);
			});
		}
	}
}
